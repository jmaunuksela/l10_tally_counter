import express from 'express';
import Counter from './counter.mjs';
import logger from './logger.mjs';
import endpoint_mw from './endpoint.mw.mjs';

const app = express();
const counter = new Counter();
// Middleware
app.use(endpoint_mw);

// Endpoints
app.get('', (req, res) => {
    res.send("Welcome!");
});
// GET /counter-increase
app.get('/counter-increase', (req, res) => {
    counter.increase();
    let value = counter.read();
    logger.info(`[COUNTER] increase ${value}`);
    res.send(`${value}`);
});
// GET /counter-read
app.get('/counter-read', (req, res) => {
    let value = counter.read();
    logger.info(`[COUNTER] read ${value}`);
    res.send(`${value}`);
});
// GET /counter-zero
app.get('/counter-zero', (req, res) => {
    counter.zero();
    let value = counter.read();
    logger.info(`[COUNTER] zeroed ${value}`);
    res.send(`${value}`);
});

app.all('*', (req, res) => {
    res.status(404).send("Resource not found.");
});

export default app;